﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace sudokuCLI
{
    class Feladvany
    {
        public string Kezdo { get; private set; }
        public int Meret { get; private set; }

        public Feladvany(string sor)
        {
            Kezdo = sor;
            Meret = Convert.ToInt32(Math.Sqrt(sor.Length));
        }

        public void Kirajzol()
        {
            for (int i = 0; i < Kezdo.Length; i++)
            {
                if (Kezdo[i] == '0')
                {
                    Console.Write(".");
                }
                else
                {
                    Console.Write(Kezdo[i]);
                }
                if (i % Meret == Meret - 1)
                {
                    Console.WriteLine();
                }
            }
        }

        public int Kitoltottseg()
        {
            return (int)(Kezdo.Count(x => x != '0') / Math.Pow(Meret, 2) * 100);
        }
    }

    class sudokuCLI
    {
        static List<Feladvany> l = new List<Feladvany>();
        static void Main(string[] args)
        {
            Beolvas("feladvanyok.txt");

            Console.WriteLine("3. feladat: Beolvasva {0} feladvány.", l.Count);
            int szam = 0;

            do
            {
                Console.Write("4. feladat: Kérem a feladvány méretét [4..9]: ");
                szam = int.Parse(Console.ReadLine());
            } while (szam < 4 || szam > 9);

            var a = l.FindAll(x => x.Meret == szam);
            Console.WriteLine("{0}x{0} méretű feladványból {1} darab van tárolva\n", szam, a.Count);
            Random r = new Random();
            int r_i = r.Next(a.Count);

            Console.WriteLine("5. feladat: A kiválasztott feladvány:\n" + a[r_i].Kezdo);

            Console.WriteLine("6. feladat: A feladvány kitöltöttsége: {0}%", a[r_i].Kitoltottseg());
            Console.WriteLine("7. feladat: A feladvány kirajzolva:");
            a[r_i].Kirajzol();


            Iras(szam);
            Console.ReadKey();
        }
        static void Iras(int szam)
        {
            string f = "sudoku" + szam + ".txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(f))
                {
                    var a = l.FindAll(x => x.Meret == szam);

                    foreach (var item in a)
                    {
                        sw.WriteLine(item.Kezdo);
                    }
                    Console.WriteLine("8. feladat: {0} állomány {1} darab feladvánnyal létrehozva.", f, a.Count);
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
        static void Beolvas(string f)
        {
            try
            {
                using (StreamReader sr = new StreamReader(f, Encoding.UTF8))
                {
                    while (!sr.EndOfStream)
                    {
                        l.Add(new Feladvany(sr.ReadLine()));
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
    }
}
