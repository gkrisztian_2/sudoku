## Sudoku érettségi feladat ##

https://dload-oktatas.educatio.hu/erettsegi/feladatok_2020tavasz_kozep/k_infoism_20maj_fl.pdf

### GUI fejlesztések ###

* Intervallumon kívül nem enged (átállítja a button állapotát kikapcsoltra)
* Kitöltöttség indikátor
* Real-time kitöltöttség indikátor (színnel)

![Kép](https://jupiterweb.space/iskola/sudokugui_img.png "Kép a megoldott feladatról")

