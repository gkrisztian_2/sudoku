﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace sudokuGUI
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            button1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int akt = Convert.ToInt16(textBox1.Text);
            if (akt < 9)
            {
                akt++;
                textBox1.Text = akt.ToString();

                /*Ha a méret nagybb lenne következő növelésnél 9-nél, akkor a button kikapcsolásra kerül*/
                if (akt == 9)
                    button2.Enabled = false;

                if (!button1.Enabled) 
                    button1.Enabled = true;
            }
            //else MessageBox.Show("A méret nem lehet kisebb 4-nél!");  // feladat által kért megoldás

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int akt = Convert.ToInt16(textBox1.Text);
            if (akt > 4)
            {
                akt--;
                textBox1.Text = akt.ToString();

                /*Ha a méret kisebb lenne következő csökkentésnél 4-nél, akkor a button kikapcsolásra kerül*/
                if (!button2.Enabled)
                    button2.Enabled = true;

                if (akt == 4)
                    button1.Enabled = false;
            }
            //else MessageBox.Show("A méret nem lehet nagyobb 9-nél!"); // feladat által kért megoldás
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            label3.Text = "Hossz: " + textBox2.Text.Length.ToString();
            int k = Kitoltottseg();
            lb_kitoltottseg.Text = $"Kitöltöttség: {k}%";
            if(k > 100)
            {
                lb_kitoltottseg.ForeColor = Color.Red;
            }
            else if(k == 100)
            {
                lb_kitoltottseg.ForeColor = Color.Green;
            }
            else
            {
                lb_kitoltottseg.ForeColor = Color.Black;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int meret = Convert.ToUInt16(textBox1.Text);
            int akthossz = textBox2.Text.Length;
            string s = "";
            if (akthossz < meret * meret)
                s = "A feladvány rövid: kell még "+ ((meret*meret)-akthossz) +" számjegy!";
            else if (akthossz > meret * meret)
                s = "A feladvány hosszú: törlendő "+(akthossz-(meret*meret)) + " számjegy!";
            else
                s = "A feladvány megfelelő hosszúságú!";
            MessageBox.Show(s + Environment.NewLine + $"Kitöltöttség: {Kitoltottseg()}%");
        }

        int Kitoltottseg()
        {
            return (int)(textBox2.Text.Count(x => x != '0') / Math.Pow(int.Parse(textBox1.Text), 2) * 100);
        }
    }
}
